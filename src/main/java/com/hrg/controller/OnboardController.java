package com.hrg.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hrg.common.entity.AgentDetails;
import com.hrg.service.OnboardService;

@RestController
@RequestMapping("/service")
public class OnboardController {

	@Autowired
	OnboardService onboardService;

	@PostMapping("/onboard")
	public void agentOnboard(@RequestBody AgentDetails agentDetails) {

		onboardService.agentOnboard(agentDetails);
	}

}
