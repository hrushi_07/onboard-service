package com.hrg.repository;

import javax.inject.Named;
import javax.inject.Singleton;

import com.hrg.common.jpa.AgentDetailsJpa;

@Named
@Singleton
public class RepositoryAccessor {
	
	private static AgentDetailsJpa agentDetailsJpa;
	
	RepositoryAccessor(AgentDetailsJpa agentDetailsJpa) {
		this.agentDetailsJpa = agentDetailsJpa;
	}

	public static AgentDetailsJpa getAgentDetailsJpa() {
		return agentDetailsJpa;
	}

}
