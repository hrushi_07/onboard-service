package com.hrg.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import com.hrg.common.entity.AgentDetails;
import com.hrg.common.jpa.AgentDetailsJpa;
import com.hrg.repository.RepositoryAccessor;


@Service
public class OnboardService {

	public void agentOnboard(AgentDetails agentDetails) {
		boolean flag = RepositoryAccessor.getAgentDetailsJpa().existsById(agentDetails.getAgentId());
		if (flag) {
			System.out.println("Agent alredy Onboarded with Id :: " + agentDetails.getAgentId());
		}
		 RepositoryAccessor.getAgentDetailsJpa().save(agentDetails);
		System.out.println("Agent saved Successfully.");
	}

}
