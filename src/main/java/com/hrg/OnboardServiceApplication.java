package com.hrg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.hrg.common.entity")
@EnableJpaRepositories(basePackages = "com.hrg.common.jpa")
@ComponentScan(basePackages = { "com.hrg" })
public class OnboardServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnboardServiceApplication.class, args);
	}

}
